# TwigBooster

TwigBooster is a Symfony Bundle that provides useful Twig extension

## Functions

- enum
- enum_cases

## Installation
*composer require starxen/twigbooster*

## Usage

See [DOCUMENTATION.md](docs/DOCUMENTATION.md)

