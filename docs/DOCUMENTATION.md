# TwigBooster documentation

## Installation

*composer require starxen/twigbooster*

Make sure the bundle is correctly added in your project

```php
// config/bundles.php
StarXen\TwigBooster\TwigBoosterBundle::class => ['all' => true]
```

## Functions

### Enum support

Example enum

```php
// src/Enum/ColorEnum.php
<?php

declare(strict_types=1);

namespace App\Enum;

enum ColorEnum: string
{
    case Red = 'red';
    case Green = 'green';
    case Blue = 'blue';

    public function getHex(): string
    {
        return match ($this) {
            self::Red => '#FF0000',
            self::Green => '#00FF00',
            self::Blue => '#0000FF'
        };
    }
}
```

#### enum

Returns an EnumUnit for use in templates

```twig
{{ enum('App\\Enum\\YourEnum::Case') }}
```

*PHP definition*

```php
public function enum(string $enum): UnitEnum
```

*Examples*

```twig
{{ enum('App\\Enum\\ColorEnum::Red').value }}
{{ enum('App\\Enum\\ColorEnum::Green').name }}
{{ enum('App\\Enum\\ColorEnum::Blue').hex }}
```

#### enum_cases

Returns an array of UnitEnum's containing all enum cases

```twig
{{ enum_cases('App\\Enum\\YourEnum') }}
```

*PHP definition*

```php
public function enumCases(string $class): array // UnitEnum[]
```

*Example*

```twig
{% for enum in enum_cases('App\\Enum\\ColorEnum') %}
    {{ enum.value }}
    {{ enum.name }}
    {{ enum.hex }}
{% endfor %}
```