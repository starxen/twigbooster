<?php

declare(strict_types=1);

namespace StarXen\TwigBooster\Twig;

use ReflectionEnum;
use ReflectionEnumUnitCase;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use UnitEnum;

class EnumExtension extends AbstractExtension
{
    public function getFunctions(): array
    {
        return [
            new TwigFunction('enum', [$this, 'enum']),
            new TwigFunction('enum_cases', [$this, 'enumCases'])
        ];
    }

    public function enum(string $enum): UnitEnum
    {
        $enumData = explode('::', $enum);
        $reflection = new ReflectionEnumUnitCase($enumData[0], $enumData[1]);

        return $reflection->getValue();
    }

    public function enumCases(string $class): array
    {
        $reflection = new ReflectionEnum($class);
        $cases = [];
        foreach ($reflection->getCases() as $case) {
            $cases[] = $case->getValue();
        }

        return $cases;
    }
}
